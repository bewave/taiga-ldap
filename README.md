# What is Taiga?

Taiga is a project management platform for startups and agile developers & designers who want a simple, beautiful tool that makes work truly enjoyable.

> [taiga.io](https://taiga.io)

# How to use this image
Thin image is a fork of the exellent [benhutchins/docker-taiga](https://github.com/benhutchins/docker-taiga) that is simply intended to make it easier to integrate with orchestration platforms such as kubernetes.

The image will be very similar but orchestration changes the way it needs to be configured and requires some more automation since you essentially are not doing docker commands yourself.

Once your container is running, use the default administrator account to login: username is `admin`, and the password is `123123`.

If you're having trouble connecting, make sure you've configured your `TAIGA_HOSTNAME`. It will default to `localhost`, which almost certainly is not what you want to use.

## Extra configuration options

Use the following environmental variables to generate a `local.py` for [taiga-back](https://github.com/taigaio/taiga-back).

  - `TAIGA_HOSTNAME=taiga.mycompany.com` (**required** set this to the server host like `taiga.mycompany.com`)
  - `TAIGA_SSL=True` (see `Enabling HTTPS` below)
  - `TAIGA_SECRET_KEY=mysecret` (set this to a random string to configure the `SECRET_KEY` value for taiga-back; defaults to an insecure random string)
  - `TAIGA_SKIP_DB_CHECK=False` (set to skip the database check that attempts to automatically setup initial database)
  - `TAIGA_ENABLE_EMAIL=True` (see `Configuring SMTP` below)

*Note*: Database variables are also required, see `Using Database server` below. These are required even when using a container for your database.

## Configure Database

The container can be linked to connect Taiga with a running [postgres](https://registry.hub.docker.com/_/postgres/) containeror use an external address.

### Using Docker container

If you want to run your database within a docker container, simply start your database server before starting your Taiga container. Here is a simple example pulled from [postgres](https://registry.hub.docker.com/_/postgres/)'s guide.

    docker run --name taiga-postgres -e POSTGRES_PASSWORD=mypassword -d postgres

### Using Database server

Use the following, **required**, environment variables for connecting to another database server:

 - `TAIGA_DB_NAME=postgres` (defaults to `postgres`)
 - `TAIGA_DB_HOST=myserver` (defaults to the address of a linked `postgres` container)
 - `TAIGA_DB_USER=postgres` (defaults to `postgres)`)
 - `TAIGA_DB_PASSWORD=mypassword` (defaults to the password of the linked `postgres` container)

If the `TAIGA_DB_NAME` specified does not already exist on the provided PostgreSQL server, it will be automatically created the the Taiga's installation scripts will run to generate the required tables and default demo data.

## Enabling HTTPS

If you want to enable support for HTTPS, you'll need to specify all of these
additional arguments to your `docker run` command.

  - `-e TAIGA_SSL=True`
  - `-v $(pwd)/ssl.crt:/etc/nginx/ssl/ssl.crt:ro`
  - `-v $(pwd)/ssl.key:/etc/nginx/ssl/ssl.key:ro`

Or create a folder called `ssl`, place your `ssl.crt` and `ssl.key` inside
this directory and then mount it with:

  - `-e TAIGA_SSL=True`
  - `-v $(pwd)/ssl/:/etc/nginx/ssl/:ro`

### HTTPS behind reverse proxies

If you have a reveser proxy that is already handling https set `TAIGA_SSL_BY_REVERSE_PROXY`:

- `-e TAIGA_SSL_BY_REVERSE_PROXY=True`

The value of `TAIGA_SSL` will then be ignored and taiga will not handle https,
it will however set all links to https.

## Configuring SMTP

If you want to use an SMTP server for emails, you'll need to specify all of
these additional arguments to your `docker run` command.

- `-e TAIGA_ENABLE_EMAIL=True`
- `-e TAIGA_EMAIL_FROM=no-reply@taiga.mycompany.net`
- `-e TAIGA_EMAIL_USE_TLS=True` (only if you want to use tls)
- `-e TAIGA_EMAIL_HOST=smtp.google.com`
- `-e TAIGA_EMAIL_PORT=587`
- `-e TAIGA_EMAIL_USER=me@gmail.com`
- `-e TAIGA_EMAIL_PASS=super-secure-pass phrase thing!`

**Note:** This can also be configured directly inside your own config file, look
at [this example](https://github.com/benhutchins/docker-taiga-example/tree/master/simple)'s
setup where it specifies a `taiga-conf/local.py`. You can then configure this
and other [settings available in Taiga](https://github.com/taigaio/taiga-back/blob/master/settings/local.py.example).

## Volumes

Uploads to Taiga go to the media folder, located by default at `/usr/src/taiga-back/media`, you can hook it to a volume:
taiga-media:/usr/src/taiga-back/media

You can also put the configuration files in a volume like so:
taiga-settings:/usr/src/taiga-back/settings
