# If you want to modify this file, I recommend check out docker-taiga-example
# https://github.com/benhutchins/docker-taiga-example
#
# Please modify this file as needed, see the local.py.example for details:
# https://github.com/taigaio/taiga-back/blob/master/settings/local.py.example
#
# Importing docker provides common settings, see:
# https://github.com/benhutchins/docker-taiga/blob/master/docker-settings.py
# https://github.com/taigaio/taiga-back/blob/master/settings/common.py

from .docker import *
import os

PUBLIC_REGISTER_ENABLED = False
DEBUG = False
TEMPLATE_DEBUG = False

## LDAP
# see https://github.com/ensky/taiga-contrib-ldap-auth
#INSTALLED_APPS += ["taiga_contrib_ldap_auth"]

#LDAP_SERVER = os.getenv('LDAP_SERVER')
#LDAP_PORT = 389

#{"error_message": "Error connecting to LDAP server: port must be an integer"}
#
# LDAP_PORT = os.getenv('LDAP_PORT')
# if LDAP_PORT is None:
#     LDAP_PORT = 389
# else:
#     LDAP_PORT = int(LDAP_PORT)


# Full DN of the service account use to connect to LDAP server and search for login user's account entry
# If LDAP_BIND_DN is not specified, or is blank, then an anonymous bind is attempated
#LDAP_BIND_DN = os.getenv('LDAP_BIND_DN')
#LDAP_BIND_PASSWORD = os.getenv('LDAP_BIND_PASSWORD')
# Starting point within LDAP structure to search for login user
#LDAP_SEARCH_BASE = os.getenv('LDAP_SEARCH_BASE')
# LDAP property used for searching, ie. login username needs to match value in sAMAccountName property in LDAP
#LDAP_SEARCH_PROPERTY = os.getenv('LDAP_SEARCH_PROPERTY')
#LDAP_SEARCH_SUFFIX = os.getenv('LDAP_SEARCH_SUFFIX')

# Names of LDAP properties on user account to get email and full name
#LDAP_EMAIL_PROPERTY = os.getenv('LDAP_EMAIL_PROPERTY')
#LDAP_FULL_NAME_PROPERTY = os.getenv('LDAP_FULL_NAME_PROPERTY')